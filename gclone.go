//go:generate goversioninfo -icon=graphics/logo/ico/icon.ico -arm -64
package main

import (
	_ "gitea.s1.thoxy.xyz/thoxy/gclone/backend/all" // import all backends
	_ "gitea.s1.thoxy.xyz/thoxy/gclone/cmd/copy"
	"github.com/rclone/rclone/cmd"
	_ "github.com/rclone/rclone/cmd/all" // import all commands
	"github.com/rclone/rclone/fs"
	_ "github.com/rclone/rclone/lib/plugin" // import plugins
)

func main() {
	fs.Version = fs.Version + "-mod2.4"
	cmd.Main()
}
